#!/bin/bash
set -euo pipefail

function usage {
    echo "usage: download.sh <parameter_profile_file> <target_dir>"
    exit 1
}

if [ $# -ne 2 ]; then
    usage
fi
PARAMETER_PROFILE_FILE=$1
TARGET_DIR=$2

if [ ! -f $PARAMETER_PROFILE_FILE ]; then
    echo "Parameter profile file not found: $PARAMETER_PROFILE_FILE"
    usage
fi


if [ ! -d $TARGET_DIR ]; then
    echo "Target dir not found: $TARGET_DIR"
    usage
fi


echo "download data to target dir: $TARGET_DIR"


# population around stops : works for France with INSEE data URL hardcoded

echo "download data to target dir: $TARGET_DIR"

# From https://www.data.gouv.fr/fr/datasets/donnees-carroyees-a-200m-sur-la-population/
DATASET_URL='https://www.data.gouv.fr/fr/datasets/r/6072929c-ba60-4a15-b797-ee3c1b20e21b'
ZIP_FILE_NAME=carroyage_insee_metro.zip

cd $TARGET_DIR
wget -q $DATASET_URL -O $ZIP_FILE_NAME
unzip $ZIP_FILE_NAME
echo "downloaded Insee population"

# Download GTFS data
GTFS_URL=`jq -r '.GTFS' $PARAMETER_PROFILE_FILE`
echo "GTFS file: $GTFS_URL"
GTFS_FILE_NAME=gtfs.zip

wget -q "$GTFS_URL" -O $GTFS_FILE_NAME

echo "download GTFS data to target dir: $TARGET_DIR/$GTFS_FILE_NAME"
echo "done."

