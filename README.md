# OpenMobility indicator Jupyter notebook 3M "population aux arrêts"

## description : [indicator.yaml file](https://gitlab.com/open-mobility-indicators/indicators/stop_pop/-/blob/main/indicator.yaml)

This python notebook produces a GeoJSON file containing around TAM stop points with the number of inhabitants residing less than 350 meters from the stop.

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute)](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook/-/blob/main/README.md)  
